# Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend:libc-glibc:cassini := "${THISDIR}:${THISDIR}/linux-yocto:"

#
# cassini kmeta
#

SRC_URI:append:libc-glibc:cassini = " file://cassini-kmeta;type=kmeta;name=cassini-kmeta;destsuffix=cassini-kmeta"

# Add gator daemon kernel configs dependencies.
KERNEL_FEATURES:append:libc-glibc:aarch64:cassini = "${@bb.utils.contains('DISTRO_FEATURES', \
        'cassini-sdk', \
        ' features/cassini/gator.scc', '', d)}"
