# Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

# Ensure grub supports the required commands
GRUB_BUILDIN:append:corstone1000 = " gzio"
