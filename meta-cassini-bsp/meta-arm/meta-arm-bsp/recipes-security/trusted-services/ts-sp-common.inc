# Copyright (c) 2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

# Common part of all Trusted Services SPs recipes

# From meta-arm
require recipes-security/trusted-services/ts-sp-common.inc

# Local overrides
require trusted-services-src.inc
require ts-uuid.inc
