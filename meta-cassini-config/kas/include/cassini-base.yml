# Copyright (c) 2022-2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

---
header:
  version: 11
  includes:
    - meta-cassini-config/kas/include/cassini-release.yml

repos:

  # Since in cassini-release.yml we pin meta-arm repo, we exclude its layers in
  # here and enable them in arm-machines.yml when included by specific kas
  # configs (e.g. n1sdp.yml)
  meta-arm:
    layers:
      meta-arm: excluded
      meta-arm-bsp: excluded
      meta-arm-toolchain: excluded

  meta-cassini:
    layers:
      meta-cassini-distro:
      meta-cassini-bsp:

  meta-openembedded:
    url: https://git.openembedded.org/meta-openembedded
    path: layers/meta-openembedded
    layers:
      meta-oe:
      meta-filesystems:
      meta-networking:
      meta-python:

  meta-security:
    url: https://git.yoctoproject.org/git/meta-security
    path: layers/meta-security
    layers:
      meta-parsec:

  meta-clang:
    url: https://github.com/kraj/meta-clang
    path: layers/meta-clang

  meta-virtualization:
    url: https://git.yoctoproject.org/git/meta-virtualization
    path: layers/meta-virtualization

  poky:
    url: https://git.yoctoproject.org/git/poky
    path: layers/poky
    layers:
      meta:
      meta-poky:

local_conf_header:
  cassini-base: |
    CONF_VERSION = "2"
    PACKAGECONFIG:remove:pn-qemu-system-native = "gtk+ sdl"

  diskmon: |
    BB_DISKMON_DIRS = "\
      STOPTASKS,${TMPDIR},1G,100K \
      STOPTASKS,${DL_DIR},1G,100K \
      STOPTASKS,${SSTATE_DIR},1G,100K \
      STOPTASKS,/tmp,100M,100K \
      HALT,${TMPDIR},100M,1K \
      HALT,${DL_DIR},100M,1K \
      HALT,${SSTATE_DIR},100M,1K \
      HALT,/tmp,10M,1K"

distro: cassini
target: unset
machine: unset

env:
  SSTATE_MIRRORS: ""
  SOURCE_MIRROR_URL: ""
  INHERIT: ""
  BB_GENERATE_MIRROR_TARBALLS: ""
  BB_NUMBER_THREADS: "${@os.cpu_count()}"
  CASSINI_ROOTFS_EXTRA_SPACE: "2000000"
  CASSINI_GENERIC_ARM64_FILESYSTEM: "1"
  CASSINI_GENERIC_ARM64_DEFAULTTUNE: "armv8a-crc"
