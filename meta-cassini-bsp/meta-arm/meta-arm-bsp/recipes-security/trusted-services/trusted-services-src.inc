# Copyright (c) 2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

# Define sources of Trusted Service and all external dependencies

# Fetch Block Storage branch for N1SDP platform
SRC_URI:remove:n1sdp = "git://git.trustedfirmware.org/TS/trusted-services.git;protocol=https;branch=integration;name=trusted-services;destsuffix=git/trusted-services "
SRC_URI:prepend:n1sdp = "git://git.trustedfirmware.org/TS/trusted-services.git;protocol=https;branch=topics/block_storage;name=trusted-services;destsuffix=git/trusted-services "
SRCREV_trusted-services:n1sdp = "4d02a83e48924377777a0d7bf58a08a800882424"
LIC_FILES_CHKSUM:n1sdp = "file://${S}/license.rst;md5=ea160bac7f690a069c608516b17997f4"
