# Copyright (c) 2023 Arm Limited and/or its affiliates.
# <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

# Check for platforms with or without secure enclave
_IS_TS_ENABLED = "${@bb.utils.contains('MACHINE_FEATURES', 'ts-crypto ts-its', \
                        '1', bb.utils.contains('MACHINE_FEATURES', 'ts-se-proxy', '1', '', d), d)}"

SRC_URI:append:cassini := "${@bb.utils.contains('_IS_TS_ENABLED', '1', \
                        ' file://0001-cassini-bsp-Enable-parse-service-to-use-TS.patch', '', d)}"
