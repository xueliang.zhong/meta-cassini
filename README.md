<!--
# Copyright (c) 2021-2022, Arm Limited.
#
# SPDX-License-Identifier: MIT
-->

# Project Cassini

Project Cassini is the open, collaborative, standards-based initiative to
deliver a seamless cloud-native software experience for devices based on Arm
Cortex-A.

## Cassini Documentation

To build the Cassini documentation locally, the following packages need to be
installed on the host (these instructions were validated in a Ubuntu 18.04
machine with Python 3.8):

```bash
    python3 -m pip install -U -r documentation/requirements.txt
```

To build and generate the documentation in html format, run:
```bash
    sphinx-build -b html -a -W documentation public
```

To render and explore the documentation, simply open
`meta-cassini/public/index.html` in a web browser.

## Repository License

The repository's standard licence is the MIT license, under which most of the
repository's content is provided. Exceptions to this standard license relate to
files that represent modifications to externally licensed works (for example,
patch files). These files may therefore be included in the repository under
alternative licenses in order to be compliant with the licensing requirements of
the associated external works.

License details may be found in the [local license file](LICENSE.rst), or as
part of the project documentation.

Contributions to the project should follow the same licensing arrangement.

## Contact

Please see the project documentation for the list of maintainers, as well as the
process for submitting contributions, raising issues, or receiving feedback and
support.
