# Copyright (c) 2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

# Corstone-1000's FVP needs images to have a extra empty space at the end or the FVP
# truncates the MMC card resulting in invalid GPT entries as the GPT last block is
# beyond the MMC cards reported size
IMAGE_TYPES += "wic.pad"

CONVERSIONTYPES += "pad"

CONVERSION_CMD:pad = "cp ${IMAGE_NAME}${IMAGE_NAME_SUFFIX}.${type} ${IMAGE_NAME}${IMAGE_NAME_SUFFIX}.${type}.pad && dd if=/dev/zero count=1024 bs=512 >> ${IMAGE_NAME}${IMAGE_NAME_SUFFIX}.${type}.pad"

IMAGE_FSTYPES:append:corstone1000-fvp = " wic.pad.gz"

# Ensure cassini-image-* also builds the firmware for corstone1000 using a different libc
do_image_complete[depends] = "${@bb.utils.contains_any('MACHINE','corstone1000-mps3 corstone1000-fvp', \
                              'corstone1000-deploy-image:do_deploy', '', d)}"
