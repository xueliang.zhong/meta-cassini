# Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT
ARG DOCKERHUB_MIRROR=

FROM ${DOCKERHUB_MIRROR}ubuntu:focal

ARG local_user=cassini-ci
ARG user_id=999
ARG group_id=999

RUN apt-get update && \
    apt-get install -y locales && \
    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG=en_US.utf8
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get install --no-install-recommends -y \
    ca-certificates \
    curl \
    git \
    gnupg2 \
    libguestfs-tools \
    linux-image-generic \
    openssh-client \
    python3 \
    python3-pip \
    python3-setuptools \
    python3-wheel \
    qemu-utils \
    software-properties-common \
    wget \
   && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 9A2D61F6BB03CED7522B8E7D6657DBE0CC86BB64 \
   && add-apt-repository ppa:rmescandon/yq \
   && apt-get update \
   && apt-get install yq -y \
   && apt-get clean \
   && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN python3 -m pip install \
    j2cli \
    j2cli[yaml] \
    pyyaml

RUN groupadd -g $group_id $local_user && useradd --no-log-init -m -d /builder -g $local_user -u $user_id $local_user \
  && chown -R $local_user:$local_user /builder \
  && cd /usr/local/bin \
  && curl -fL https://getcli.jfrog.io/v2 | sh \
  && chmod a+x /usr/local/bin/*

ENV LIBGUESTFS_BACKEND=direct
