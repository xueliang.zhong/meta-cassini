# Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Overlay image containing bmaptool"
LICENSE = "MIT"

inherit core-image
inherit nopackages

PACKAGE_INSTALL = "bmap-tools"

IMAGE_FEATURES = ""
IMAGE_LINGUAS = ""

IMAGE_ROOTFS_SIZE = "0"
IMAGE_ROOTFS_EXTRA_SPACE = "0"

IMAGE_FSTYPES += "tar.bz2"
