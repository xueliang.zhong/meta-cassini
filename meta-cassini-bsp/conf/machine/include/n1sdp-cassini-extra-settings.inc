# Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

# FFA driver
MACHINE_FEATURES:append = " arm-ffa"

# Trusted services secure partitions
MACHINE_FEATURES:append = " ts-crypto ts-storage ts-its ts-attestation ts-block-storage"
