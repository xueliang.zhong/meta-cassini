# Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend:libc-glibc := "${THISDIR}:${THISDIR}/linux-yocto:"

SRC_URI:append:libc-glibc:corstone1000 = " \
    file://extfs.cfg \
    file://cgroups.cfg \
    file://mmc.cfg \
    file://container.cfg \
    file://network.cfg \
    ${@bb.utils.contains('DISTRO_FEATURES', \
            'cassini-sdk', \
            'file://sdk.cfg ', '', d)} \
    "
