# Copyright (c) 2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

DESCRIPTION = "Trusted Services block storage service provider"

require ts-sp-common.inc

SP_UUID = "${BLOCK_STORAGE_UUID}"

OECMAKE_SOURCEPATH="${S}/deployments/block-storage/config/default-${TS_ENV}"
