..
 # Copyright (c) 2023 Arm Limited or its affiliates. All rights reserved.
 #
 # SPDX-License-Identifier: MIT

##########################################
Getting Started with Arm Corstone-1000 FVP
##########################################

This document explains how to boot the Cassini distro on the Arm Corstone-1000 FVP.

*****
Build
*****

The provided kas configuration file meta-cassini-config/kas/corstone1000-fvp.yml can be used to build images
that are targeting the Corstone-1000 FVP.

.. note::
  To build and run any image for the Corstone-1000 FVP the user has to
  accept its |EULA|_, which can be done by executing
  the following command in the build environment:

  .. code-block:: console

    export FVP_CORSTONE1000_EULA_ACCEPT=True


Building FVP images
===================

To build a Corstone-1000 FVP image:

  .. code-block:: console

    kas build --update meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/corstone1000-fvp.yml

***
Run
***

The resulting Cassini distribution image can be logged into as ``root`` user.

Running the FVP
===============

To start the FVP and get the console:

  .. code-block:: console

    kas shell -c="../layers/meta-arm/scripts/runfvp --verbose --console" \
    meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/corstone1000-fvp.yml

.. _reproduce_run-time_integration_tests:

**********
Validation
**********

The following validation tests can be performed on the Cassini Reference Stack:

  * System Integration Tests:

    * Cassini Architecture Stack:

      .. code-block:: console

        TESTIMAGE_AUTO=1 kas build meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/corstone1000-fvp.yml

      The previous test takes around 2 minutes to complete.

      A similar output should be printed out:

      .. code-block:: console

        NOTE: Executing Tasks
        Creating terminal default on host_terminal_0
        default: Waiting for login prompt
        RESULTS:
        RESULTS - linuxboot.LinuxBootTest.test_linux_boot: PASSED (23.70s)
        SUMMARY:
        cassini-image-base () - Ran 1 test in 23.704s
        cassini-image-base - OK - All required tests passed (successes=1, skipped=0, failures=0, errors=0)
