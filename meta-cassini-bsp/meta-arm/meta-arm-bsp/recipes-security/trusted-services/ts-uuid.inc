# Copyright (c) 2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

# Trusted Services SPs canonical UUIDs

BLOCK_STORAGE_UUID = "63646e80-eb52-462f-ac4f-8cdf3987519c"
