# Copyright (c) 2022-2023 Arm Limited and/or its affiliates.
# <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

# Config specifc to the cassini-test distro feature, enabled using
# DISTRO_FEATURES

DISTRO_FEATURES:append:libc-glibc = " ptest"

EXTRA_IMAGE_FEATURES:append:libc-glibc = " debug-tweaks"

IMAGE_INSTALL:append:libc-glibc = " jfrog-cli \
                                 container-engine-integration-tests-ptest \
                                 k3s-integration-tests-ptest \
                                 user-accounts-integration-tests-ptest \
                                 ${@bb.utils.contains('DISTRO_FEATURES',\
                                 'cassini-parsec', 'parsec-simple-e2e-tests-ptest', '', d)} \
                                 "

EXTRA_USERS_PARAMS:prepend:libc-glibc = "usermod -aG teeclnt ${CASSINI_TEST_ACCOUNT};"
